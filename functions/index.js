const functions = require('firebase-functions')

const admin = require('firebase-admin')
admin.initializeApp()

const db = admin.firestore()

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info('Hello logs!', { structuredData: true })
  response.send('Hello from Firebase!')
})

exports.handleCreateUser = functions.auth.user().onCreate(user => {
  /* Extract useful data. */
  const {
    uid,
    displayName,
    photoURL
  } = user

  const userData = {
    uid,
    displayName,
    photoURL
  }

  /* Add the user data to the database, with their user ID as the key. */
  return db.collection('users').doc(user.uid).set(userData)
})

exports.handleDeleteUser = functions.auth.user().onDelete(user => {
  /* TODO delete linked data. */
  return db.collection('users').doc(user.uid).delete()
})
