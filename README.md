# SpokeIt SLP Web App

The SpokeIt SLP Web App is a patient management app for the SpokeIt project. It
runs in a browser, and is written using React, Material-UI, and Firebase. If you
are new to these, you should start off by becoming familiar with React,
especially React Hooks, from the links below:
 - <https://reactjs.org/tutorial/tutorial.html>
 - <https://reactjs.org/docs/hooks-intro.html>

After you are familiar, you can follow the steps below to get a local
development environment set up on your computer. Note that you will also need to
be added to the SpokeIt Google Cloud project, which can be done by asking one of
the people working on the team.

Thank you for helping with the project! Hopefully you will have fun and learn a
lot in working on the app :)

## Usage

There is a somewhat long process you will need to do to get the project working
on your local computer. Here is a brief rundown of what you will need:
 1. If you are using Windows, it would be a good idea to install a Linux VM and
    work on the VM instead of on Windows. Ubuntu 20.04 running in VirtualBox is
    a good choice. You can also install cd  (WSL)
    instead, which is like a Linux virtual machine integrated directly into
    Windows. Either way, Windows can be fairly difficult to work with, so in my
    opinion it is best to use Ubuntu or Mac.
 2. Set up an SSH key for your GitHub account if you haven't already. Here is a
    tutorial on how to do this:
    <https://devconnected.com/how-to-setup-ssh-keys-on-github/>.
 3. Install git, node.js, yarn, and firebase-tools onto your computer. This can
    be done by installing Node.js from <https://nodejs.org/en/download/>, using
    it to install yarn via `sudo npm install --global yarn`, and then using
    `sudo yarn global add firebase-tools` to install the firebase emulators.
 4. Clone the SpokeIt SLP Web App repo via `git clone
    git@github.com:kurniawan-hci-group/SpokeItSLPWebApp.git`
 5. Install the dependencies for each project via `yarn`. To do this, change
    into the frontend directory and run `yarn`. Next, change into the root
    directory, and then into the functions directory, and run `yarn
    --ignore-engines`. Note that we are using `--ignore-engines` so that we can
    just use one version of Node.js for local development, but the functions
    themselves will actually run on an older version of Node. You will also want
    to run `yarn` in the root directory so that you can run `yarn makedocs` to
    make the documentation.
 6. Login to firebase by running `firebase login`.
 7. Start the firebase emulators from the root directory by running `firebase
    emulators:start`
 8. Start the React project by changing into the frontend directory and running
    `yarn start`

And that's the basic setup! If you want, there is also HTML documentation stored
in the docs folder, which you can open with a web browser. To generate the
documentation, go to the root directory and run `yarn makedocs`. There are a few
more details below, but also feel free to reach out to someone on the team, or
attend a cuckoo session if you need help :)

In addition, you can check README.md in the frontend folder, which has info
about different scripts you can run from within the frontend folder.

### Using the Firebase emulators

Here are some additional details about Firebase, and the firebase emulators.

#### Starting the emulators

This project will use Firebase. To decrease difficulty in testing the app and
avoid corrupting real data, we can use the Firebase emulator instead of Firebase
itself. This does require a slight amount of set up though. You can read about
the emulator suite [here](https://firebase.google.com/docs/emulator-suite). To
simply start the emulator so you can test out the site, do the following in your
terminal:
 1. **Install and set up Firebase CLI if you haven't already.** Since we are
    using yarn, you can do this with the following command: `yarn global add
    firebase-tools` After running this command, you should be able to run
    `firebase login` or `firebase login --no-localhost` if you are developing on
    a remote computer.
 2. **Change into this directory.**
 3. **Start the emulators** by running `firebase emulators:start`.

That should be everything. Once you have started the emulator, you should see
`Running in emulator mode. Do not use with production credentials.` in any page
of the React app. You should also be able to sign in using the UI.

#### Deployment

Eventually we will want to deploy the app. Luckily, firebase-tools has a feature
that lets us do this from the command line! Simply run `firebase deploy`. This
**should not** be done yet though, until we get a good build working.

## Contributing

Since there are many people working on this app, here are some rules to follow.
 1. Before committing, run `yarn lint --fix` so that we can have consistent
    coding style. We are using the [standardjs](https://standardjs.com/) style.
    No need to read through the whole style guide though, simply run `yarn lint
    --fix` and fix any inconsistencies that are not fixed automatically.
 2. Use jsdoc comments as needed so that documenation can be built from the
    source code. If you make a change that requires the docs to be rebuilt, run
    `yarn makedocs` to build the docs before you commit.
 3. Use React function components with hooks instead of React class components.

That's basically all! Thank you for your help.
