/**
 * @module hooks
 */

/* Firebase. */
import firebase from 'firebase/app'

import * as converters from './backend/converters'

/* React. */
import {
  useState,
  useEffect
} from 'react'

/* React Router. */
import {
  useParams
} from 'react-router-dom'

/**
 * Returns the current patients info.
 *
 * @example
 * const patient = usePatient()
 * console.log(patient.uid)
 */
export function usePatient () {
  /* Get the patient ID from React Router. */
  const { patientId } = useParams()
  return useUserData(patientId)
}

/**
 * Returns a list of patients for the currently signed in clinician.
 *
 * @return {UserData[]}
 *
 * @example
 * const patients = usePatients()
 * console.log(patients[0].uid)
 */
export function usePatients () {
  const db = firebase.firestore()
  const user = useUser()
  const userData = useUserData(user ? user.uid : undefined)
  const [patients, setPatients] = useState()

  useEffect(() => {
    if (user && userData) {
      const linkedUsers = userData.linkedUsers || []

      /* Query the users who are linked to this account, and who this account is
       * linked to. */
      return (
        db
          .collection('users')
          .withConverter(converters.userData)
          .where('linkedUsers', 'array-contains', user.uid)
          .onSnapshot(snapshot => {
            /* Add the patients to a list. */
            const patients = []
            snapshot.forEach(doc => {
              const patient = doc.data()
              /* Only include users that have been accepted by the clinician. */
              if (linkedUsers.includes(patient.uid)) {
                patients.push(doc.data())
              }
            })
            setPatients(patients)
          })
      )
    }
  }, [user, userData, setPatients, db])

  return patients
}

/**
 * Returns the currently signed in user (or null) from firebase. Returns
 * undefined if firebase is still initializing.
 */
export function useUser () {
  const [user, setUser] = useState()

  /* Listen to sign in changes. */
  useEffect(() => {
    const cleanup = firebase.auth().onAuthStateChanged(user => {
      setUser(user)
    })
    return () => cleanup()
  }, [])

  return user
}

/**
 * Returns the userData from Firestore for the given user, or null if uid is
 * null. This data can be used to find linked accounts, and other info.
 *
 * @param {String} uid - The user ID of the user to retrieve the data from.
 */
export function useUserData (uid) {
  const db = firebase.firestore()
  const [userData, setUserData] = useState(uid === null ? null : undefined)
  useEffect(() => {
    if (uid) {
      return (
        db
          .collection('users')
          .withConverter(converters.userData)
          .doc(uid)
          .onSnapshot(snapshot => {
            setUserData(snapshot.data())
          })
      )
    } else {
      if (uid === null) {
        setUserData(null)
      } else {
        setUserData(undefined)
      }
    }
  }, [uid, setUserData, db])

  return userData
}

/**
 * Returns a list of patients that have requested to join the currently logged
 * in clinician.
 *
 * @returns {UserData[]}
 */
export function useInvites () {
  const db = firebase.firestore()
  const user = useUser()
  const userData = useUserData(user ? user.uid : undefined)
  const [patients, setPatients] = useState()

  useEffect(() => {
    if (user && userData) {
      const linkedUsers = userData.linkedUsers || []

      /*
       * Query the users who are linked to this account, but have not been
       * accepted by the clinician yet.
       */
      return (
        db
          .collection('users')
          .where('linkedUsers', 'array-contains', user.uid)
          .withConverter(converters.userData)
          .onSnapshot(snapshot => {
            /* Add the patients to a list. */
            const patients = []
            snapshot.forEach(doc => {
              const patient = doc.data()
              /*
               * Only include users that have not been confirmed by the
               * clinician.
               */
              if (!linkedUsers.includes(patient.uid)) {
                patients.push(doc.data())
              }
            })
            setPatients(patients)
          })
      )
    }
  }, [user, userData, setPatients, db])

  return patients
}
