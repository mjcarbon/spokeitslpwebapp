/* MUI Components. */
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

import { useUser, useUserData } from '../hooks'

/* Firebase. */
import firebase from 'firebase/app'

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}))

/*
 * Prompts the user for their type of account. onUserTypeChange is called when
 * the user selects the type with either 'patient', or 'clinician'
 */
function UserTypePage ({ user, onUserTypeChange }) {
  const classes = useStyles()

  const handleSignOut = () => {
    firebase.auth().signOut()
  }

  return (
    <Box m={2}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant='h4' gutterBottom>
            SpokeIt Web Portal
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant='h5' gutterBottom>
            Welcome!
          </Typography>
          <Grid item xs={12}>
            <Box m={1}>
              <Avatar
                alt={user.displayName}
                src={user.photoURL}
                style={{ margin: 'auto' }}
                className={classes.large}
              />
            </Box>
          </Grid>
          <Typography paragraph>
            Welcome <b>{user.displayName}</b>! Thank you for creating an
            account. Before you can move on to the site, please let us know a
            bit about yourself so that we can fine-tune your experience!
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Button
            variant='contained'
            color='primary'
            fullWidth
            onClick={() => { onUserTypeChange('parent') }}
          >
            I am a parent
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            variant='contained'
            fullWidth
            onClick={() => { onUserTypeChange('clinician') }}
          >
            I am a clinician
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button onClick={handleSignOut} fullWidth>Sign out</Button>
        </Grid>
      </Grid>
    </Box>
  )
}

/*
 * This page is where the user chooses whether they have a patient, clinician,
 * or parent account.
 */
export default function UserInfoForm () {
  const user = useUser()
  const userData = useUserData(user ? user.uid : null)

  const isLoading = (user === undefined)

  const handleUserTypeChange = (type) => {
    if (userData) {
      userData
        .setType(type)
        .sync()
    }
  }

  const loadingUi = (
    <Container maxWidth='sm'>
      <Box m={2}>
        <Typography paragraph>
          Loading...
        </Typography>
      </Box>
    </Container>
  )

  const ui = user && (
    <Container maxWidth='sm'>
      <UserTypePage
        user={user}
        onUserTypeChange={handleUserTypeChange}
      />
    </Container>
  )

  return isLoading ? loadingUi : ui
}
