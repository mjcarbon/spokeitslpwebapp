/* Firebase. */
import firebase from 'firebase/app'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth'

/* MUI Components. */
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import React, { useEffect, useState } from "react";
import {app} from '../base'
import { NewAlbumForm } from '../NewAlbumForm'


const authUiConfig = {
  /* Popup signin flow rather than redirect flow. */
  signInFlow: 'popup',
  /* Display Google, and email as sign in providers. */
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
      signInMethod: firebase.auth.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD
    }
  ]
}
const db = app.firestore()
export default function SignInPage () {
  const [albums, setAlbums] = useState([
  ])
  useEffect(() => {
    db.collection('albums').onSnapshot((snapshot) => {
      const tempAlbums = []
      snapshot.forEach(doc => {
        tempAlbums.push(doc.data());
      })
      setAlbums(tempAlbums)
        
      });
    })
  return (
    <Container maxWidth='sm'>
      <Box m={2}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant='h4' gutterBottom>
              SpokeIt Web Portal
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant='h5' gutterBottom>
              Please sign in
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              paragraph
            >
              Welcome to the SpokeIt SLP Web Portal! Please sign in using one of
              the methods below.
            <section>
              <aside>
              </aside>
            </section>
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Container>
              <StyledFirebaseAuth
                uiConfig={authUiConfig}
                firebaseAuth={firebase.auth()}
              />
            </Container>
          </Grid>
        </Grid>
      </Box>
      <NewAlbumForm />
    </Container>

  )
}
