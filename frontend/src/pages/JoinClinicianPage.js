import assert from 'assert'

/* MUI Components. */
import Avatar from '@material-ui/core/Avatar'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import { makeStyles } from '@material-ui/core/styles'

/* React Router. */
import {
  useParams
} from 'react-router-dom'

import { useUser, useUserData } from '../hooks'

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}))

/**
 * Page displayed when the user is going to join their clinician from an invite
 * link.
 */
export default function JoinClincianPage () {
  const classes = useStyles()

  /* Get the data of the clinician who we are joining. */
  const { clinicianId } = useParams()
  assert(typeof clinicianId === 'string', 'invalid clinician ID')
  const clinician = useUserData(clinicianId)

  /* Get the current user's userData. */
  const user = useUser()
  const userData = useUserData(user ? user.uid : null)

  const isConfirmed = (
    userData &&
    clinician &&
    userData.getLinkedUsers().includes(clinician.uid)
  )

  const handleConfirm = () => {
    if (userData && clinician.uid) {
      userData
        .addLinkedUser(clinician.uid)
        .sync()
    }
  }

  const confirmUi = clinician && (
    <>
      <Grid item xs={12}>
        <Typography paragraph>
          You are about to share your account
          with <b>{clinician.displayName}</b>. Are you sure? This
          action cannot be undone!
        </Typography>
      </Grid>
      <Grid item container spacing={2}>
        <Grid item>
          <Button
            variant='contained'
            color='primary'
            onClick={handleConfirm}
          >
            Confirm
          </Button>
        </Grid>
      </Grid>
    </>
  )

  const successUi = clinician && (
    <Grid item xs={12}>
      <Typography paragraph>
        Your account has been linked
        to <b> {clinician.displayName} </b>'s
        clincian account. Thank you!
      </Typography>
    </Grid>
  )

  const isLoaded = clinician && userData

  return (
    <Container maxWidth='sm'>
      <Box m={2}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant='h5' gutterBottom>
              Join clinician
            </Typography>
          </Grid>
          {isLoaded
            ? (
              <>
                <Grid item xs={12}>
                  <Box m={1}>
                    <Avatar
                      alt={clinician.displayName}
                      src={clinician.photoURL}
                      style={{ margin: 'auto' }}
                      className={classes.large}
                    />
                  </Box>
                </Grid>
                {isConfirmed ? successUi : confirmUi}
              </>
              )
            : (
              <Grid item xs={12}>
                <Typography paragraph>
                  Please wait...
                </Typography>
              </Grid>
              )}
        </Grid>
      </Box>
    </Container>
  )
}
