/* React. */
import {
  useState
} from 'react'

/* React Router. */
import {
  generatePath,
  Link as RouterLink
} from 'react-router-dom'

/* MUI Components. */
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import IconButton from '@material-ui/core/IconButton'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'

import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'

/* MUI Icons. */
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import CheckIcon from '@material-ui/icons/Check'

import CopyableLink from '../components/CopyableLink'

import { useUser, useUserData, usePatients, useInvites } from '../hooks'

/* Firebase. */
import firebase from 'firebase/app'

function PatientListItem ({ patient }) {
  return (
    <ListItem
      button
      component={RouterLink}
      to={`/patient/${patient.uid}`}
    >
      <ListItemAvatar>
        <Avatar
          alt={patient.displayName} src={patient.photoURL}
        />
      </ListItemAvatar>
      <ListItemText> {patient.displayName} </ListItemText>
    </ListItem>
  )
}

function InviteListItem ({ patient }) {
  const db = firebase.firestore()
  const user = useUser()
  const userData = useUserData(user ? user.uid : null)
  const linkedUsers = userData ? (userData.linkedUsers || []) : []

  const handleAccept = () => {
    db.collection('users').doc(user.uid).set({
      ...userData,
      linkedUsers: [...linkedUsers, patient.uid]
    })
  }

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar
          alt={patient.displayName} src={patient.photoURL}
        />
      </ListItemAvatar>
      <ListItemText> {patient.displayName} </ListItemText>
      <ListItemSecondaryAction>
        <Tooltip title='Accept' onClick={handleAccept}>
          <IconButton edge='end'>
            <CheckIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title='Decline'>
          <IconButton edge='end'>
            <ClearIcon />
          </IconButton>
        </Tooltip>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

function InviteDialog ({ onClose, ...rest }) {
  const user = useUser()
  const { protocol, host } = window.location
  const baseUrl = `${protocol}//${host}`
  /* Create the absolute URL. */
  const link = (
    !user
      ? ''
      : baseUrl + generatePath(
        '/join/clinician/:clinicianId',
        { clinicianId: user.uid }
      )
  )
  return (
    <Dialog {...rest} onClose={onClose}>
      <DialogTitle>Invite Patients</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Please share the link below with your patients to have them link their
          patient account with your clinician account.
        </DialogContentText>
        <CopyableLink to={link} />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default function Dashboard () {
  const patients = usePatients()
  const invites = useInvites()

  const isLoading = (patients === undefined || invites === undefined)

  const [inviteOpen, setInviteOpen] = useState(false)

  const currentPatientsUi = patients && (
    <Grid item xs={12}>
      <Box m={2}>
        <Typography variant='h6' component='h2'>Current Patients</Typography>
        <List width={1}>
          {patients.map(x => <PatientListItem patient={x} key={x.uid} />)}
          {patients.length === 0 && (
            <ListItem>
              <ListItemText>
                No patients yet!
              </ListItemText>
            </ListItem>
          )}
        </List>
      </Box>
    </Grid>
  )

  const invitesUi = invites && (
    <Grid item xs={12}>
      <Box m={2}>
        <Typography variant='h6' component='h2'>Invitations</Typography>
        <List width={1}>
          {invites.map(x => <InviteListItem patient={x} key={x.uid} />)}
          <ListItem button onClick={() => setInviteOpen(true)}>
            <ListItemIcon><AddIcon /></ListItemIcon>
            <ListItemText>Invite Patients</ListItemText>
          </ListItem>
        </List>
      </Box>
    </Grid>
  )

  const patientsUi = (
    <>
      {currentPatientsUi}
      {invitesUi}
    </>
  )

  const loadingUi = (
    <Grid item xs={12}>
      <Box m={2}>
        <Typography paragraph>
          Loading...
        </Typography>
      </Box>
    </Grid>
  )

  return (
    <Box m={2} width={1}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Toolbar>
            <Typography variant='h3' component='h1'>All Patients</Typography>
          </Toolbar>
          <Divider />
        </Grid>
        {isLoading
          ? loadingUi
          : patientsUi}
      </Grid>
      <InviteDialog
        open={inviteOpen}
        onClose={() => setInviteOpen(false)}
      />
    </Box>
  )
}
