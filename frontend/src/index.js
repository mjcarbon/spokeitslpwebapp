import React from 'react'
import ReactDOM from 'react-dom'

/* Firebase. */
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'

import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'

/* Firebase config. */
const firebaseConfig = {
  apiKey: 'AIzaSyCyNIs1fjX_zJ0jA7o5Sgk0XGJ2awKTRvY',
  authDomain: 'spokeit.firebaseapp.com',
  databaseURL: 'https://spokeit-default-rtdb.firebaseio.com',
  projectId: 'spokeit',
  storageBucket: 'spokeit.appspot.com',
  messagingSenderId: '573466675705',
  appId: '1:573466675705:web:bd650d1c062c89a91ed528'
}

/* Initialize firebase. */
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)

}

/* If we are connected via localhost, assume we are using an emulator. */
if (window.location.hostname === 'localhost') {
  /* Weirdly inconsistent Firebase API, but oh well. */
  firebase.auth().useEmulator('http://localhost:9099')
  firebase.firestore().useEmulator('localhost', 8080)
  firebase.functions().useEmulator('localhost', 5001)
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
