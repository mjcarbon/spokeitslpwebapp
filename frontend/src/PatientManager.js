
/* MUI Components. */
import Box from '@material-ui/core/Box'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

/* React Router. */
import {
  Switch,
  Route,
  Link as RouterLink,
  generatePath
} from 'react-router-dom'

import { usePatient } from './hooks'

function FillerText () {
  return (
    <>
      <Typography paragraph>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Rhoncus dolor purus
        non enim praesent elementum facilisis leo vel. Risus at ultrices mi
        tempus imperdiet. Semper risus in hendrerit gravida rutrum quisque non
        tellus. Convallis convallis tellus id interdum velit laoreet id donec
        ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl
        suscipit adipiscing bibendum est ultricies integer quis. Cursus euismod
        quis viverra nibh cras.  Metus vulputate eu scelerisque felis imperdiet
        proin fermentum leo. Mauris commodo quis imperdiet massa tincidunt. Cras
        tincidunt lobortis feugiat vivamus at augue. At augue eget arcu dictum
        varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt.
        Lorem donec massa sapien faucibus et molestie ac.
      </Typography>
      <Typography paragraph>
        Consequat mauris nunc congue nisi vitae suscipit. Fringilla est
        ullamcorper eget nulla facilisi etiam dignissim diam. Pulvinar elementum
        integer enim neque volutpat ac tincidunt. Ornare suspendisse sed nisi
        lacus sed viverra tellus. Purus sit amet volutpat consequat mauris.
        Elementum eu facilisis sed odio morbi. Euismod lacinia at quis risus sed
        vulputate odio. Morbi tincidunt ornare massa eget egestas purus viverra
        accumsan in. In hendrerit gravida rutrum quisque non tellus orci ac.
        Pellentesque nec nam aliquam sem et tortor. Habitant morbi tristique
        senectus et. Adipiscing elit duis tristique sollicitudin nibh sit.
        Ornare aenean euismod elementum nisi quis eleifend. Commodo viverra
        maecenas accumsan lacus vel facilisis. Nulla posuere sollicitudin aliquam
        ultrices sagittis orci a.
      </Typography>
    </>
  )
}

function FillerPage (props) {
  const {
    patient
  } = props

  return (
    <>
      <Typography variant='h1'> {props.title} </Typography>
      {patient && (
        <Typography paragraph> Patient name: {patient.displayName} </Typography>
      )}
      <FillerText />
    </>
  )
}

function PatientDashboard (props) {
  /* TODO */
  return (
    <FillerPage title='Dashboard' {...props} />
  )
}

function PatientReports (props) {
  /* TODO */
  return (
    <FillerPage {...props} />
  )
}

function PatientRecordings (props) {
  /* TODO */
  return (
    <FillerPage {...props} />
  )
}

function PatientCurriculum (props) {
  /* TODO */
  return (
    <FillerPage {...props} />
  )
}

function PatientNotifications (props) {
  /* TODO */
  return (
    <>
      <Typography variant='h1'> Push Notifications </Typography>
      <Typography variant='h1' gutterBottom/>
      <Typography variant='h3'> Send a Message </Typography>
      <Typography variant='h3' gutterBottom/>
      <TextField
          id="outlined-multiline-static"
          multiline
          rows={4}
          style={{ width: "50%" }}
          variant="outlined"
      />
      <Typography gutterBottom align="right"/>
      <Button variant="contained" color="primary">
          Submit
      </Button>
    </>
  )
}

/* Routes for the patient manager. */
const routes = [
  {
    path: '/patient/:patientId',
    title: 'Dashboard',
    component: PatientDashboard,
    exact: true
  },
  {
    path: '/patient/:patientId/reports',
    title: 'Reports',
    component: PatientReports
  },
  {
    path: '/patient/:patientId/recordings',
    title: 'Recordings',
    component: PatientRecordings
  },
  {
    path: '/patient/:patientId/curriculum',
    title: 'Manage Curriculum',
    component: PatientCurriculum
  },
  {
    path: '/patient/:patientId/notifications',
    title: 'Push Notifications',
    component: PatientNotifications
  }
]

/* The sidebar for the patient manager. */
function PatientSidebar (props) {
  const {
    patient
  } = props
  return (
    <List>
      {
        routes.map(route => (
          <ListItem
            button
            component={RouterLink}
            to={generatePath(route.path, { patientId: patient.uid })}
            key={route.path}
          >
            <ListItemText> {route.title} </ListItemText>
          </ListItem>
        ))
      }
    </List>
  )
}

/**
 * React component that shows information about the current patient. Sets up
 * some basic routes that allow different patients to be viewed, and holds each
 * of the pages for managing the patient (Dashboard, Reports, Manage Curriculum,
 * etc.)
 */
export default function PatientManager () {
  /* Get patient information. */
  const patient = usePatient()
  const isLoading = !patient

  const breadcrumbs = !isLoading && (
    <Grid item xs={12}>
      <Toolbar>
        <Breadcrumbs>
          <Link to='/' component={RouterLink}> Patients </Link>
          <Link
            to={`/patient/${patient.uid}`}
            component={RouterLink}
          >
            {patient.displayName}
          </Link>

          <Switch>
            {routes.map(route => (
              <Route path={route.path} exact={route.exact} key={route.path}>
                <Typography color='textPrimary'> {route.title} </Typography>
              </Route>
            ))}
          </Switch>
        </Breadcrumbs>
      </Toolbar>
      <Divider />
    </Grid>
  )

  const sidebar = !isLoading && (
    <Grid item>
      <PatientSidebar patient={patient} />
    </Grid>
  )

  const pages = !isLoading && (
    <Grid item sm>
      <Switch>
        {routes.map(route => (
          <Route path={route.path} exact={route.exact} key={route.path}>
            <route.component title={route.title} patient={patient} />
          </Route>
        ))}
      </Switch>
    </Grid>
  )

  const ui = (
    <Grid container spacing={3}>
      {breadcrumbs}
      {sidebar}
      {pages}
    </Grid>
  )

  const loadingUi = (
    <Typography paragraph>
      Loading...
    </Typography>
  )

  return (
    <Box m={2}>
      {isLoading ? loadingUi : ui}
    </Box>
  )
}
