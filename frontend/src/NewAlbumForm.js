import React, { useState, useEffect } from "react";
import { app } from "./base.js";
import firebase from "firebase/app";
import "firebase/storage";
import { Howl } from 'howler';

const db = app.firestore();
let dLink = ""; 
let audioTotal = 0;
let audioNames = [];
let testName = [{id: 1, meta: {name: "bob"}}]
// const renderAudio = () =>{
//     let storageRef = firebase.storage().ref()   
//     let spaceRef = storageRef.child('AudioFiles/m4a')
//     spaceRef.listAll().then((res) => {
//         res.prefixes.forEach((itemRef) => {
//             console.log(itemRef.name)
//         })
//     })
// }

function listAll(folder) {
    const storageRef = firebase.storage().ref();
  
    // [START storage_list_all]
    // Create a reference under which you want to list
    var listRef = storageRef.child(folder);
  
    // Find all the prefixes and items.
    listRef.listAll()
      .then((res) => {
        res.prefixes.forEach((folderRef) => {
          // All the prefixes under listRef.
          // You may call listAll() recursively on them.
        });
        res.items.forEach((itemRef) => {
            itemRef.getDownloadURL().then((url => {
                console.log("download URL " + url );
                console.log(itemRef.name);
                itemRef["url"] = url;
                audioNames.push(itemRef); 
                audioTotal += 1;
            }))
            
          // All the items under listRef.
        });
      }).catch((error) => {
        // Uh-oh, an error occurred!
      });
    // [END storage_list_all]
  } 
function printAudio(){
    audioNames.forEach(name => {
        console.log(name);
    });
}

export function NewAlbumForm() {
    const [x, setX] = useState(0)
    const [y, setY] = useState(0)
    const logMousePosition = e => {
        console.log('Mouse event')
        setX(e.clientX)
        setY(e.clientY)
    }
    const listItems = audioNames.map((d) => 
    <li key={d.name}>
        {d.name}
        
        <audio controls>
            <source src={d.url} type="audio/wav" / >
        </audio>
    </li>
    );
    useEffect(() => {
        console.log('useEffect called')
        window.addEventListener('mousemove', logMousePosition)
        listAll("AudioFiles/");
    }, [])
    return (
        <div>
            hooks {x} and {y}
            <audio id ="aud">
                <source id="audSRC" type ="audio/wav" />

            </audio>
            {listItems}
            <button onClick={printAudio}>print audio</button>
        </div>
    )
}
export default NewAlbumForm; 

