/**
 * @module converters
 */

import { makeUserData } from '.'

/**
 * An object that converts from firebase to UserData object and vice versa.
 *
 * @example
 * import * as converters from './backend/converters'
 * const db = firebase.firestore()
 * db.collection('users').withConverter(converters.userData)
 */
export const userData = {
  toFirestore (data) {
    return data.json()
  },
  fromFirestore (doc) {
    return makeUserData(doc.data())
  }
}
