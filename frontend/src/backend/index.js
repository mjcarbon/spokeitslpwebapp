/**
 * @module backend
 */

import assert from 'assert'

/* Firebase. */
import firebase from 'firebase/app'
import * as converters from './converters'

/**
 * Data about a user retrieved from the database.
 *
 * @property {String} displayName - The name of the user
 * @property {String} type - The type of user, either patient or clinician
 * @property {String} uid - The unique ID of the user
 * @property {String} photoURL - The URL of the user's avatar
 * @property {String[]} linkedUsers - The list of the UIDs of the users linked
 *                                    to this account
 */
class UserData {
  /**
   * Creates user data. This function should not be called directly.  Instead,
   * prefer {@link makeUserData}, or use converters.userData when converting to
   * and from firebase.
   */
  constructor ({ type, displayName, uid, photoURL, linkedUsers }) {
    this.displayName = displayName
    this.type = type
    this.uid = uid
    this.photoURL = photoURL
    this.linkedUsers = linkedUsers
  }

  /**
   * Set the linked accounts for the given user data, and return a new UserData
   * object.
   *
   * @param {UserData} userData - The current user data
   * @param {String[]} linkedUsers - The new list of linked accounts
   * @return {UserData} this
   */
  setLinkedUsers (linkedUsers) {
    assert(Array.isArray(linkedUsers), 'setLinkedAccounts requires an array')
    this.linkedUsers = linkedUsers
    return this
  }

  /**
   * Return the linked users or an empty list if not available.
   *
   * @return {UserData[]}
   */
  getLinkedUsers () {
    return this.linkedUsers || []
  }

  /**
   * Add the given user ID to the list of linked users.
   * @param {String} uid - The user ID to add
   * @return {UserData}
   */
  addLinkedUser (uid) {
    assert(typeof uid === 'string', 'invalid user ID')
    return this.setLinkedUsers([...this.getLinkedUsers(), uid])
  }

  /**
   * Set the user account type (parent or clinician).
   *
   * @param {UserData} userData - The user data to copy
   * @param {String} type - The new type, must be 'parent', or 'clinician'
   *
   * @return {UserData} this
   */
  setType (type) {
    assert(typeof type === 'string')
    assert(
      type === 'parent' || type === 'clinician',
      'User type must be "patient" or "clinician"'
    )
    this.type = type
    return this
  }

  /**
   * Update the user on the server.
   *
   * @return {UserData} this
   */
  sync () {
    assert(typeof this.uid === 'string')
    const db = firebase.firestore()
    db
      .collection('users')
      .withConverter(converters.userData)
      .doc(this.uid)
      .set(this)
    return this
  }

  /**
   * Return a serializable version of this object.
   */
  json () {
    const ret = {}
    /* Cannot serialize undefined values. */
    const copyField = (key) => {
      if (this[key] !== undefined) {
        ret[key] = this[key]
      }
    }
    const fields = ['uid', 'type', 'displayName', 'photoURL', 'linkedUsers']
    fields.forEach(copyField)
    return ret
  }
}

/**
 * Create user data using the specified arguments.
 *
 * Note: you should use converters.userData instead
 */
export function makeUserData (...args) {
  return new UserData(...args)
}
