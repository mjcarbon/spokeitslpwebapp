/* React. */
import {
  useState,
  useRef
} from 'react'

/* MUI Components. */
import IconButton from '@material-ui/core/IconButton'
import LinkIcon from '@material-ui/icons/Link'
import Snackbar from '@material-ui/core/Snackbar'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'
import TextField from '@material-ui/core/TextField'

/**
 * A link that can be copied, for example for an invitation.
 */
export default function CopyableLink ({ to }) {
  const ref = useRef()
  const [snackbarOpen, setSnackbarOpen] = useState(false)
  const handleClick = () => {
    ref.current.select()
  }
  const handleButtonClick = () => {
    navigator.clipboard.writeText(to).then(() => {
      setSnackbarOpen(true)
    })
  }
  return (
    <Toolbar>
      <TextField
        value={to}
        inputRef={ref}
        onClick={handleClick}
        fullWidth
      />
      <Tooltip title='Copy link'>
        <IconButton edge='end' onClick={handleButtonClick}>
          <LinkIcon />
        </IconButton>
      </Tooltip>
      <Snackbar
        onClose={() => { setSnackbarOpen(false) }}
        open={snackbarOpen}
        message='Link copied!'
        anchor={{ horizontal: 'center', vertical: 'bottom' }}
      />
    </Toolbar>
  )
}
