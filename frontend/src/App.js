/* React. */
import {
  useState
} from 'react'

/* MUI Components. */
import Avatar from '@material-ui/core/Avatar'
import Box from '@material-ui/core/Box'
import CssBaseline from '@material-ui/core/CssBaseline'
import Drawer from '@material-ui/core/Drawer'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import Tooltip from '@material-ui/core/Tooltip'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import { makeStyles } from '@material-ui/core/styles'

/* MUI Icons. */
import HelpIcon from '@material-ui/icons/Help'
import PeopleIcon from '@material-ui/icons/People'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'

/* React Router. */
import {
  Switch,
  BrowserRouter as Router,
  Route,
  Redirect,
  Link as RouterLink
} from 'react-router-dom'

/* App Components. */
import PatientManager from './PatientManager'
import Dashboard from './pages/Dashboard'
import SignInPage from './pages/SignInPage'
import UserInfoForm from './pages/UserInfoForm'
import JoinClincianPage from './pages/JoinClinicianPage'

/* Firebase. */
import firebase from 'firebase/app'

import { useUser, useUserData } from './hooks'

const drawerWidth = 56

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    overflowX: 'hidden'
  }
}))

function AppSidebarButton (props) {
  const {
    text,
    icon,
    ...rest
  } = props
  return (
    <Tooltip title={text} placement='right'>
      <ListItem button {...rest}>
        <ListItemIcon> {icon} </ListItemIcon>
      </ListItem>
    </Tooltip>
  )
}

function MyAccount (props) {
  const signOut = () => {
    firebase.auth().signOut()
  }
  const user = useUser()

  return (
    <SwipeableDrawer {...props}>
      <List>
        {user && (
          <>
            <ListItem>
              <Box
                textAlign='center'
                fontWeight='fontWeightBold'
                m={1}
              >
                <Box m={1}>
                  <Avatar
                    alt={user.displayName} src={user.photoURL}
                    style={{ margin: 'auto' }}
                  />
                </Box>
                {user.displayName}
              </Box>
            </ListItem>
            <ListItem button onClick={signOut}>
              <ListItemText> Sign out </ListItemText>
            </ListItem>
          </>
        )}
      </List>
    </SwipeableDrawer>
  )
}

function AppSidebar (props) {
  const classes = useStyles()
  const [myAccountOpen, setMyAccountOpen] = useState(false)
  return (
    <>
      <Drawer variant='permanent' className={classes.drawer}>
        <List style={{ width: drawerWidth, overflowX: 'hidden' }}>
          <AppSidebarButton
            text='All Patients'
            icon={<PeopleIcon />}
            to='/'
            component={RouterLink}
          />
          <AppSidebarButton
            text='My Account'
            icon={<AccountCircleIcon />}
            onClick={() => setMyAccountOpen(true)}
          />
          <AppSidebarButton text='Help' icon={<HelpIcon />} />
        </List>
      </Drawer>
      <MyAccount
        open={myAccountOpen}
        onOpen={() => setMyAccountOpen(true)}
        onClose={() => setMyAccountOpen(false)}
      />
    </>
  )
}

/**
 * React Component app for the clinician. Contains basic routes for clinician accounts.
 */
function ClinicianApp () {
  const classes = useStyles()
  return (
    <Router>
      <div className={classes.root}>
        <Switch>
          <Route path='/'>
            <CssBaseline />
            <AppSidebar />
            <Switch>
              <Route path='/patient/:patientId'>
                <PatientManager />
              </Route>
              <Route path='/'>
                <Dashboard />
              </Route>
            </Switch>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

/**
 * React Component app for parent accounts. At the moment it is basically the same as the
 * clinician app except that it always redirects to show the logged in user's
 * data, instead of a list of patients.
 */
function ParentApp () {
  const classes = useStyles()
  const user = useUser()

  return (
    <Router>
      <div className={classes.root}>
        <Switch>
          <Route path='/join/clinician/:clinicianId'>
            <JoinClincianPage />
          </Route>
          <Route path='/'>
            <CssBaseline />
            <AppSidebar />
            <Switch>
              <Route path='/patient/:patientId'>
                <PatientManager />
              </Route>
              <Route path='/'>
                {user && (
                  <Redirect to={`/patient/${user.uid}`} />
                )}
              </Route>
            </Switch>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

/**
 * React component that serves as the main entry point to the application. It
 * handles sign in, and shows the correct application view, either the {@link
 * ClinicianApp} or {@link ParentApp} depending on the type of the currently
 * signed in user's account.
 */
function App () {
  const classes = useStyles()
  const user = useUser()
  const userData = useUserData(user ? user.uid : undefined)

  if (userData && userData.type !== undefined) {
    if (userData.type === 'clinician') {
      return (
        <ClinicianApp />
      )
    } else if (userData.type === 'parent') {
      return (
        <ParentApp />
      )
    } else {
      throw new Error('Unknown user type: ' + userData.type)
    }
  } else if (!userData) {
    /*
     * If the user is not logged in, show the sign in page.
     *
     * Note: user === null if the user is not logged in, user === undefined if
     * the auth object is still initializing. We use user === null here so that
     * a blank page is displayed if the auth object is still initializing.
     */
    return (
      <div className={classes.root}>
        <CssBaseline />
        {user === null && (
          <SignInPage />
        )}
      </div>
    )
  } else {
    /*
     * If the user has not specified the type of their account, prompt them with
     * the questionnaire.
     */
    return (
      <div className={classes.root}>
        <CssBaseline />
        <UserInfoForm />
      </div>
    )
  }
}

export default App
